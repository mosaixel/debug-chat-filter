import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbModule } from 'xng-breadcrumb';

import { GenericViewComponent } from './generic-view.component';
import { GenericViewRoutingModule } from './generic-view-routing.module';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { ThemeModule, lightTheme, darkTheme } from '../theme';

import { PageTitleComponent } from './page-title/page-title.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { RefreshFilterComponent } from './refresh-filter/refresh-filter.component';
import { FiltersComponent } from './filters/filters.component';
import { LanguageFilterComponent } from './language-filter/language-filter.component';
import { ContentFilterComponent } from './content-filter/content-filter.component';
import { EnvironmentFilterComponent } from './environment-filter/environment-filter.component';
import { AnalysisFilterComponent } from './analysis-filter/analysis-filter.component';
import { PhraseDiagnoseComponent } from './phrase-diagnose/phrase-diagnose.component';
import { PhraseDetailsComponent } from './phrase-details/phrase-details.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    GenericViewComponent,
    PageTitleComponent,
    BreadcrumbComponent,
    RefreshFilterComponent,
    FiltersComponent,
    LanguageFilterComponent,
    ContentFilterComponent,
    EnvironmentFilterComponent,
    AnalysisFilterComponent,
    PhraseDiagnoseComponent,
    PhraseDetailsComponent
  ],
  imports: [
    CommonModule,
    GenericViewRoutingModule,
    SharedComponentsModule,
    BreadcrumbModule,
    FontAwesomeModule,
    ThemeModule.forRoot({
      themes: [lightTheme, darkTheme],
      active: 'dark'
    }),
  ]
})
export class GenericViewModule { }
