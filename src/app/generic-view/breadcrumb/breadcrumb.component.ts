import { Component, OnInit } from '@angular/core';
import { faLink, faChevronRight } from '@fortawesome/free-solid-svg-icons'; // FontAwesome icons

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.less']
})
export class BreadcrumbComponent implements OnInit {

  faLink = faLink; // FontAwesome icon
  faChevronRight = faChevronRight; // FontAwesome icon

  constructor() { }

  ngOnInit() {
  }

}
